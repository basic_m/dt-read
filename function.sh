
#!/bin/bash
#将一个目录下的一些文件移动到另一个目录下

if [ $# -lt 1 ]
    then echo 'please input dev or build'
    exit
fi

mkdir -p ./dist/$1/mp-weixin/cloudfunctions
cp -R `find ./cloudfunctions -type d -path ./cloudfunctions/\*/node_modules -prune -o -print | sed 1d ` ./dist/$1/mp-weixin/cloudfunctions