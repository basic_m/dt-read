# DT-READ

![logo](/docs/logo/logo.png 'logo')

使用通用 APP 框架[uni-app](https://uniapp.dcloud.io/)，利用[Thor-ui](https://github.com/dingyong0214/ThorUI-uniapp)，搭建的微信小程序。

后台使用的是微信小程序自带的云开发。
云开发数据库中的数据源解析方式导出文件路径`/src/static/database_export.json`。云函数路径在`/cloudfunctions`。

> 所需数据库如下图所示：
> ![云开发数据库](/docs/interface/云开发数据库.png '云开发数据库')
> 管理员标志在数据库`config`中的`admin=true`字段。

通过解析小说网站结构，相关的数据显示在微信小程序。

可以动态在数据库添加小说网站的解析，解析是动态的。只要在数据库里添加数据源的解析，即可在小程序中正常使用该数据源。不需要额外写代码。

> 已有的小说源：

- [笔趣阁儿](http://www.biquger.com)
- [小说族](http://www.xiaoshuozu.com)
- [天籁小说网](https://www.23txt.com)
- ~~[笔趣阁备用站](https://www.biquge.cc)~~
- ~~[全本小说网](https://www.qb5200.tw)~~
- [小说书网](https://www.xiaoshuo530.com)
- [斗破苍穹小说网](https://dpcq1.com)

## 部分界面如下图所示

<div style="display:flex;flex-wrap:wrap;">
<img width="280" src="./docs/interface/主界面-我的书架.jpg" alt="主界面-我的书架" title="主界面-我的书架">
<img width="280" src="./docs/interface/搜索书籍.jpg" alt="搜索书籍" title="搜索书籍" />
<img width="280" src="./docs/interface/阅读界面-菜单.jpg" alt="阅读界面-菜单" title="阅读界面-菜单" />
<img width="280" src="./docs/interface/个人中心.jpg" alt="个人中心" title="个人中心" />
<img width="280" src="./docs/interface/设置界面.jpg" alt="设置界面" title="设置界面" />
</div>

## 安装依赖

```shell
yarn install
```

### 开发环境运行

```bash
yarn dev:mp-weixin
```

如果需要本地调试小程序云函数，则可以运行`sh function.sh dev`，可以一次性把云函数复制到指定的目录

然后用微信开发者工具导入小程序，目录在`/dist/dev/mp-weixin/`

### 生产环境运行

```shell
yarn build:mp-weixin
```

如果需要本地调试小程序云函数，则可以运行`sh function.sh build`，可以一次性把云函数复制到指定的目录

然后用微信开发者工具导入小程序，目录在`/dist/build/mp-weixin/`

## [怎么发布自己的小程序？](https://gitee.com/wtto00/dt-read/issues/I1HSBK)

## 添加小说解析源

按照[小说解析源规则](https://gitee.com/wtto00/dt-read/issues/I1Z1F9)，仿照[database_export.json](https://gitee.com/wtto00/dt-read/blob/master/src/static/database_export.json)，修改部分字段，来添加自己的小说解析源
